package org.gopas.proxydemo;

import org.gopas.proxydemo.service.AddressService;
import org.gopas.proxydemo.service.PersonService;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

// https://docs.spring.io/spring-framework/docs/current/reference/html/core.html#aop
@SpringBootApplication
public class ProxyDemoApplication {

    public static void main(String[] args) {
        ApplicationContext applicationContext = SpringApplication.run(ProxyDemoApplication.class, args);
        PersonService personService = applicationContext.getBean(PersonService.class);
        System.out.println(String.format("[ Main ] Person service class: [ %s ] ", personService.getClass().getSimpleName()));

        personService.timeTrackedMethod();

        // check what happens when no annotation is added
        AddressService addressService = applicationContext.getBean(AddressService.class);
        System.out.println(String.format("[ Main ] Address service class: [ %s ] ", addressService.getClass().getSimpleName()));
    }
}
