package org.gopas.proxydemo.service;

public class ContactServiceImpl implements ContactService {
    @Override
    public String getContact() {
        if (isAccessible()) {
            return "My contact is: pavel.seda@gopas.cz";
        } else {
            return "My contact is: UNKNOWN.";
        }
    }

    @Override
    public boolean isAccessible() {
        return true;
    }
}
