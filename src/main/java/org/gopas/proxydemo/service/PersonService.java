package org.gopas.proxydemo.service;

import org.gopas.proxydemo.annotations.TrackTime;
import org.springframework.stereotype.Service;

@Service
public class PersonService {

    // if I declare the method final, the CGLIB proxy ignores that method
    @TrackTime
    public void timeTrackedMethod() {
        System.out.println("timeTrackedMethod is called.");
        timeTrackedMethod2();
    }

    @TrackTime
    public void timeTrackedMethod2() {
        System.out.println("TimeTrackedMethod2 is called... in PersonService");
    }

}
