package org.gopas.proxydemo.service;

public interface ContactService {

    String getContact();

    boolean isAccessible();
}
