package org.gopas.proxydemo;

import org.gopas.proxydemo.aop.TracingAdvice;
import org.gopas.proxydemo.service.ContactService;
import org.gopas.proxydemo.service.ContactServiceImpl;
import org.springframework.aop.framework.ProxyFactory;

public class ProxyFactoryDemoApplication {

    public static void main(String[] args) {
        ProxyFactory proxyFactory = new ProxyFactory(new ContactServiceImpl());
        proxyFactory.addInterface(ContactService.class);

        proxyFactory.addAdvice(new TracingAdvice());

        ContactService contactService = (ContactService) proxyFactory.getProxy();
        System.out.println(String.format("1. Object class: [ %s] ", contactService.getClass().getCanonicalName()));
        System.out.println(String.format("2. Is contact available: [ %s]", contactService.isAccessible()));
        String contact = contactService.getContact();
        System.out.println("3. " + contact);
    }
}
