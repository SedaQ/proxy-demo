package org.gopas.proxydemo.proxypatternjdkproxy;

import java.util.List;

public class RoleServiceProxy implements RoleService {

    private RoleService roleService;

    public RoleServiceProxy(RoleService roleService) {
        this.roleService = roleService;
    }

    @Override
    public List<String> getRoles() {
        // Advice implementation is performed here
        System.out.println("RoleServiceProxy (JDK Proxy): getRoles() wrapper over getRoles()");
        return roleService.getRoles();
    }

    @Override
    public void printInvocation() {
        System.out.println("RoleServiceProxy: printInvocation");
    }
}
