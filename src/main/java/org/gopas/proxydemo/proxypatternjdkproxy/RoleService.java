package org.gopas.proxydemo.proxypatternjdkproxy;

import java.util.List;

public interface RoleService {

    List<String> getRoles();

    void printInvocation();
}
