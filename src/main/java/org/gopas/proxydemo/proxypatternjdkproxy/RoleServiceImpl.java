package org.gopas.proxydemo.proxypatternjdkproxy;

import java.util.List;

public class RoleServiceImpl implements RoleService {

    @Override
    public List<String> getRoles() {
        System.out.println("RoleServiceImpl: getRoles()");
        printInvocation();
        return List.of("USER", "ADMIN");
    }

    @Override
    public void printInvocation() {
        System.out.println("RoleServiceImpl: printInvocation()");
    }
}
