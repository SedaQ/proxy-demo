package org.gopas.proxydemo.proxypatternjdkproxy.dynamicproxy;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.util.List;

public class RoleServiceInvocationHandler implements InvocationHandler {

    private RoleService roleService;

    public RoleServiceInvocationHandler(RoleService roleService) {
        this.roleService = roleService;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        System.out.println("JDK Proxy Dynamic: Before -- " + method.getName());
        Object result = method.invoke(roleService, args);
        System.out.println("JDK Proxy Dynamic: After -- " + method.getName());
        return result;
    }
}
