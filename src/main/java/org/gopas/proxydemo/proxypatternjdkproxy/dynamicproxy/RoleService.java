package org.gopas.proxydemo.proxypatternjdkproxy.dynamicproxy;

import java.util.List;

public interface RoleService {

    List<String> getRoles();

    void printInvocation();
}
