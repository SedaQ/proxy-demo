package org.gopas.proxydemo.proxypatternjdkproxy.dynamicproxy;

// Proxy Pattern design (JDK proxy, CGLIB proxy: https://blog.devgenius.io/demystifying-proxy-in-spring-3ab536046b11)

import java.lang.reflect.Proxy;
import java.util.List;

public class ProxyPatternJdkProxyDemoApp {
    public static void main(String[] args) {
        RoleService roleService = (RoleService) Proxy.newProxyInstance(
                RoleService.class.getClassLoader(), RoleServiceImpl.class.getInterfaces(),
                new RoleServiceInvocationHandler(new RoleServiceImpl())
        );
        List<String> roles = roleService.getRoles();
        roles.stream()
                .forEach(r -> System.out.println(r));
    }
}
