package org.gopas.proxydemo.proxypatternjdkproxy;

// Proxy Pattern design (JDK proxy, CGLIB proxy: https://blog.devgenius.io/demystifying-proxy-in-spring-3ab536046b11)

public class ProxyPatternJdkProxyDemoApp {
    public static void main(String[] args) {
        RoleService roleService = new RoleServiceProxy(new RoleServiceImpl());

        roleService.getRoles();
    }
}
