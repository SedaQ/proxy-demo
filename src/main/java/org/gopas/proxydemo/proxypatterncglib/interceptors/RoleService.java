package org.gopas.proxydemo.proxypatterncglib.interceptors;

import java.util.List;

public class RoleService {
    public List<String> getRoles() {
        System.out.println("CGLib -- RoleService: getRoles()");
        printInvocation();
        return List.of("USER", "ADMIN");
    }

    public void printInvocation() {
        System.out.println("CGLib -- RoleService: printInvocation()");
    }

}
