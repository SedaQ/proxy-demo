package org.gopas.proxydemo.proxypatterncglib.interceptors;

// Proxy Pattern design (JDK proxy, CGLIB proxy: https://blog.devgenius.io/demystifying-proxy-in-spring-3ab536046b11)

import org.springframework.cglib.proxy.Enhancer;
import org.springframework.cglib.proxy.MethodInterceptor;

// Enhancer subclass does not call calling super: https://stackoverflow.com/a/11583641
public class ProxyPatternCglibInterceptorDemoApp {

    public static void main(String[] args) {
        Enhancer enhancer = new Enhancer();
        enhancer.setSuperclass(RoleService.class);
        enhancer.setCallback((MethodInterceptor) (obj, method, arguments, proxy) -> {
            return proxy.invoke(obj, arguments);
//            if (method.getDeclaringClass() != Object.class && method.getReturnType() == String.class) {
//                return "Hello Tom!";
//            } else {
//                return proxy.invokeSuper(obj, arguments);
//            }
        });
        RoleService roleServiceEnhancer = (RoleService) enhancer.create();
        roleServiceEnhancer.getRoles();

//        RoleService roleService = new RoleService();
//        MethodInterceptor methodInterceptorHandler = new RoleServiceMethodInterceptor(roleService);
//        RoleService roleServiceEnhancer = (RoleService) Enhancer.create(RoleService.class, methodInterceptorHandler);
//        roleServiceEnhancer.getRoles();
    }
}
