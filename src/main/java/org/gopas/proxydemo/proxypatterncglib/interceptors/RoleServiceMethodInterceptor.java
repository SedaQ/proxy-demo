package org.gopas.proxydemo.proxypatterncglib.interceptors;

import org.springframework.cglib.proxy.MethodInterceptor;
import org.springframework.cglib.proxy.MethodProxy;

import java.lang.reflect.Method;

public class RoleServiceMethodInterceptor implements MethodInterceptor {

    private RoleService roleService;

    public RoleServiceMethodInterceptor(RoleService roleService) {
        this.roleService = roleService;
    }

    public Object intercept(Object o, Method method, Object[] args, MethodProxy methodProxy) throws Throwable {
        System.out.println("BEFORE");
        Object object = method.invoke(roleService, args);
        System.out.println("AFTER");
        return object;
    }
}
