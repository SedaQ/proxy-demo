package org.gopas.proxydemo.proxypatterncglib;

import java.util.List;

public class RoleService {

    public List<String> getRoles() {
        System.out.println("CGLib: RoleServiceImpl: getRoles()");
        printInvocation();
        return List.of("USER", "ADMIN");
    }

    public void printInvocation() {
        System.out.println("RoleServiceImpl: printInvocation()");
    }

}
