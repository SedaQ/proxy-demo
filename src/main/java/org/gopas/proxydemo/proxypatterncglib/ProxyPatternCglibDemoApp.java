package org.gopas.proxydemo.proxypatterncglib;

// Proxy Pattern design (JDK proxy, CGLIB proxy: https://blog.devgenius.io/demystifying-proxy-in-spring-3ab536046b11)

public class ProxyPatternCglibDemoApp {
    public static void main(String[] args) {
        RoleService roleService = new RoleServiceProxy();

        roleService.getRoles();
    }
}
