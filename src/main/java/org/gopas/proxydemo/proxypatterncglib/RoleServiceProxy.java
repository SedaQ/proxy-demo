package org.gopas.proxydemo.proxypatterncglib;

import java.util.List;

public class RoleServiceProxy extends RoleService {

    private RoleService delegate;

    @Override
    public List<String> getRoles() {
        System.out.println("CGLib invocation of RoleServiceProxy wrapper");
        // Advice implementation is performed here
        return delegate.getRoles();
    }

    @Override
    public void printInvocation() {
        System.out.println("RoleServiceProxy: printInvocation");
    }
}
