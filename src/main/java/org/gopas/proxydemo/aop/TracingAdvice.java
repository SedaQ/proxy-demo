package org.gopas.proxydemo.aop;

import org.springframework.aop.MethodBeforeAdvice;

import java.lang.reflect.Method;

// MethodBeforeAdvice allows us to write code that will be executed before the invocation of the proxyfied object's method
public class TracingAdvice implements MethodBeforeAdvice {

    public void before(Method method, Object[] args, Object target) throws Throwable {
        System.out.println(String.format("Calling method [ %s ]", method.getName()));
    }

}