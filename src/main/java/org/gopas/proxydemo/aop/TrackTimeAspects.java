package org.gopas.proxydemo.aop;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;

import java.time.Instant;

@Aspect
@Component
public class TrackTimeAspects {

    @Around("@annotation(org.gopas.proxydemo.annotations.TrackTime)")
    public void trackTimeAround(ProceedingJoinPoint joinPoint) throws Throwable {
        Instant startTime = Instant.now();
        System.out.println("Start time of the call..: " + startTime);

        Object objectToReturn = joinPoint.proceed(); // call of the actual method

        Instant endTime = Instant.now();
        System.out.println("End time of the call..: " + endTime);
    }

}
